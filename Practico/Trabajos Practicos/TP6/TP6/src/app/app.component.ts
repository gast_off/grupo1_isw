import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CarritoService } from './services/carrito/carrito.service';
import { BdService } from './services/bd/bd.service';
import { Producto } from './classes/producto';

import {
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';


declare var bootstrap: any
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  ciudades: string[] = ["Córdoba", "San Francisco", "Villa Gral. Belgrano"];
  formasDePago: string[] = ["Efectivo", "Tarjeta de Débito/Crédito"];
  momentosRecepcion: string[] = ["Lo antes posible", "En otro momento"];
  meses: string[] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
  years: string[] = this.generarYears();
  banderaCarrito = false;
  minFechaRecepcion!: string | null;
  maxFechaRecepcion!: string | null;

  formaPagoSeleccionada: string = "";
  momentoRecepcionSeleccionado: string = "";

  submitted = false;

  FormRegistro: FormGroup = new FormGroup({

    Calle: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(55),
    ]),

    Numero: new FormControl(null, [
      Validators.required,
      Validators.pattern('[0-9]{1,4}'),
    ]),

    Referencia: new FormControl('', [
      Validators.minLength(4),
      Validators.maxLength(55),
    ]),

    Ciudad: new FormControl('', [
      Validators.required
    ]),

    MomentoRecepcion: new FormControl('', [
      Validators.required
    ]),

    FormaPago: new FormControl('', [
      Validators.required
    ]),

    Monto: new FormControl(null, [
      Validators.nullValidator,
    ]),

    NumTarjeta: new FormControl(null, [
      Validators.nullValidator,
    ]),

    NombreApellido: new FormControl('', [
      Validators.nullValidator,
    ]),

    MesVencimiento: new FormControl('', [
      Validators.nullValidator,
    ]),

    YearVencimiento: new FormControl('', [
      Validators.nullValidator,
    ]),

    CVC: new FormControl(null, [
      Validators.nullValidator,
    ]),

    FechaRecepcion: new FormControl('', [
      Validators.nullValidator,
    ])
  });

  constructor(public carritoService: CarritoService, public bdService: BdService, private pd: DatePipe) { }

  generarYears() {
    let arrayYears = [];
    let a = new Date().getFullYear();
    for (let i = a; i < a + 10; i++) {
      arrayYears.push(i.toString())
    };
    return arrayYears;
  }

  ngOnInit() {
    let minFechaRec = new Date((String)(this.pd.transform(new Date(), "yyyy-MM-dd HH:mm")));
    minFechaRec.setMinutes(minFechaRec.getMinutes() + 30);
    let maxFechaRec = new Date();
    maxFechaRec.setDate(minFechaRec.getDate() + 5);

    this.minFechaRecepcion = this.pd.transform(minFechaRec, "yyyy-MM-ddTHH:mm");
    this.maxFechaRecepcion = this.pd.transform(maxFechaRec, "yyyy-MM-ddTHH:mm");
  }

  ngAfterViewInit() {
    this.inicializarTooltips();
  }

  inicializarTooltips() {
    let tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-tooltip="tooltip"]'));
    tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl)
    });
  }

  agregarACarrito(item: Producto) {
    this.banderaCarrito = true;
    this.carritoService.agregarACarrito(item);
    this.crearValidadoresEfectivo();
  }

  limpiarCarrito() {
    this.banderaCarrito = false;
    this.carritoService.limpiarCarrito();
    this.limpiarDatos();
  }

  getTotalProductoEnPedido() {
    return this.carritoService.detalles.reduce((acum, detalleA) => {
      return detalleA.getCantidad() + acum;
    }, 0);
  }

  getTotalCarrito() {
    return this.carritoService.detalles.reduce((acum, detalleA) => {
      return detalleA.getCantidad() * detalleA.getProducto().getPrecio() + acum;
    }, 0)
  }

  soloInteger(event: any) {
    event.target.value = event.target.value.replaceAll(/[^0-9]+/g, "");
  }

  limpiarValidadoresTarjeta() {
    this.FormRegistro.get("NumTarjeta")?.clearValidators();
    this.FormRegistro.get("NombreApellido")?.clearValidators();
    this.FormRegistro.get("MesVencimiento")?.clearValidators();
    this.FormRegistro.get("YearVencimiento")?.clearValidators();
    this.FormRegistro.get("CVC")?.clearValidators();

    this.actualizarValidadoresTarjeta();
  }

  actualizarValidadoresTarjeta() {
    this.FormRegistro.get("NumTarjeta")?.updateValueAndValidity();
    this.FormRegistro.get("NombreApellido")?.updateValueAndValidity();
    this.FormRegistro.get("MesVencimiento")?.updateValueAndValidity();
    this.FormRegistro.get("YearVencimiento")?.updateValueAndValidity();
    this.FormRegistro.get("CVC")?.updateValueAndValidity();
  }

  limpiarValidadoresEfectivo() {
    this.FormRegistro.get("Monto")?.clearValidators();
    this.FormRegistro.get("Monto")?.updateValueAndValidity();
  }

  crearValidadoresEfectivo() {
    const total = this.getTotalCarrito();
    this.FormRegistro.get("Monto")?.setValidators([Validators.required, Validators.min(total), Validators.max((Math.ceil(total / 1000)) * 1000)]);
    this.FormRegistro.get("Monto")?.updateValueAndValidity();
  }

  crearValidadoresTarjeta() {
    this.FormRegistro.get("NumTarjeta")?.setValidators([Validators.required, Validators.pattern('[0-9]{12,19}')/* , this.validarTarjeta.bind(this) */]);
    this.FormRegistro.get("NombreApellido")?.setValidators([Validators.required, Validators.minLength(4), Validators.maxLength(55)]);
    this.FormRegistro.get("MesVencimiento")?.setValidators([Validators.required, this.validarMesVencimiento.bind(this)]);
    this.FormRegistro.get("YearVencimiento")?.setValidators([Validators.required, this.validarYearVencimiento.bind(this)]);
    this.FormRegistro.get("CVC")?.setValidators([Validators.required, Validators.pattern('[0-9]{3,4}')]);

    this.actualizarValidadoresTarjeta();
  }

  setFormaPago(event: any) {
    this.formaPagoSeleccionada = event.target.value;

    if (this.formaPagoSeleccionada === "Efectivo") {
      this.limpiarValidadoresTarjeta();
      this.crearValidadoresEfectivo();
    } else {
      this.limpiarValidadoresEfectivo();
      this.crearValidadoresTarjeta();
    }
  }

  limpiarValidadoresEnOtroMomento() {
    this.FormRegistro.get("FechaRecepcion")?.clearValidators();
    this.FormRegistro.get("FechaRecepcion")?.updateValueAndValidity();
  }

  crearValidadoresEnOtroMomento() {
    this.FormRegistro.get("FechaRecepcion")?.setValidators([Validators.required, this.validarFechaRecepcion.bind(this)]);
    this.FormRegistro.get("FechaRecepcion")?.updateValueAndValidity();
  }

  setMomentoRecepcion(event: any) {
    this.momentoRecepcionSeleccionado = event.target.value;
    if (this.momentoRecepcionSeleccionado === "En otro momento") {
      this.crearValidadoresEnOtroMomento();
    } else {
      this.limpiarValidadoresEnOtroMomento()
    }
  }

  confirmarPedido() {
    this.submitted = true;
    if (this.FormRegistro.valid) {
      $('#carritoModal').modal('hide');
      $("#exitoModal").modal("show");
      this.limpiarCarrito();
    }
  }

  limpiarDatos() {
    this.submitted = false;
    this.formaPagoSeleccionada = "";
    this.momentoRecepcionSeleccionado = "";
    this.FormRegistro.reset();
  }

  // validarTarjeta(elemento: FormControl) {
  //   if (elemento.value) {
  //     if (!elemento.value.match(/^(51|52|53|54|55)/g)) {
  //       return { invalidCard: true };
  //     }
  //   }
  //   return null;
  // }

  validarFechaRecepcion(elemento: FormControl) {
    if (elemento.value) {
      let texto = elemento.value
      let date = new Date(texto.replace("T", " "));
      let dateMin = new Date((String)(this.minFechaRecepcion?.replace("T", " ")));
      let dateMax = new Date((String)(this.maxFechaRecepcion?.replace("T", " ")));
      if (date < dateMin || date > dateMax) {
        return { invalidDate: true };
      }
    }
    return null;
  }

  validarMesVencimiento(elemento: FormControl) {
    let yearVencimiento = this.FormRegistro.get("YearVencimiento");
    if (elemento.value && yearVencimiento?.value !== "") {
      let texto = elemento.value
      let date = new Date();
      if (parseInt(yearVencimiento?.value) === date.getFullYear() && parseInt(texto) < date.getMonth() + 1) {
        yearVencimiento?.setErrors({ invalidDate: true });
        return { invalidDate: true };
      } else {
        if (yearVencimiento?.touched && yearVencimiento?.hasError("required"))
          yearVencimiento?.setErrors({ invalidDate: false, required: true });
        else
          yearVencimiento?.setErrors(null);
      }
    }
    return null;
  }

  validarYearVencimiento(elemento: FormControl) {
    let mesVencimiento = this.FormRegistro.get("MesVencimiento")
    if (elemento.value && mesVencimiento?.value !== "") {
      let texto = elemento.value
      let date = new Date();
      if (parseInt(texto) === date.getFullYear() && parseInt(mesVencimiento?.value) < date.getMonth() + 1) {
        mesVencimiento?.setErrors({ invalidDate: true });
        return { invalidDate: true };
      } else {
        if (mesVencimiento?.touched && mesVencimiento?.hasError("required"))
          mesVencimiento?.setErrors({ invalidDate: false, required: true });
        else
          mesVencimiento?.setErrors(null);
      }
    }
    return null;
  }

}

