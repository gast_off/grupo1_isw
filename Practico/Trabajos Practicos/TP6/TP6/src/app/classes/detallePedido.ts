import { Producto } from "./producto"

export class DetallePedido {
    private producto: Producto;
    private cantidad: number;

    constructor(producto: Producto, cantidad: number) {
        this.producto = producto;
        this.cantidad = cantidad;
    }

    setProducto(producto: Producto) {
        this.producto = producto;
    }

    getProducto() {
        return this.producto;
    }

    setCantidad(cantidad: number) {
        this.cantidad = cantidad;
    }

    getCantidad() {
        return this.cantidad;
    }
}