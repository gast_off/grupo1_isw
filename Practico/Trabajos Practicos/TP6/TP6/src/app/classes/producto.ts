export class Producto {
    private id: number;
    private nombre: string;
    private precio: number;
    private descripcion: string

    constructor(id: number, nombre: string, precio: number, descripcion: string) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
    }

    setId(id: number) {
        this.id = id;
    }

    getId() {
        return this.id;
    }

    setNombre(nombre: string) {
        this.nombre = nombre;
    }

    getNombre() {
        return this.nombre;
    }

    setPrecio(precio: number) {
        this.precio = precio;
    }

    getPrecio() {
        return this.precio;
    }

    setDescripcion(descripcion: string) {
        this.descripcion = descripcion;
    }

    getDescripcion() {
        return this.descripcion;
    }
}