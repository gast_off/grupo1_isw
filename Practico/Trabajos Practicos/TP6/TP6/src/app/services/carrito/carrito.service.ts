import { Injectable } from '@angular/core';
import { Producto } from '../../classes/producto';
import { DetallePedido } from 'src/app/classes/detallePedido';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {
  detalles: DetallePedido[] = [];

  agregarACarrito(producto: Producto) {
    let detalleRepetido = this.detalles.find((detalle) => {
      return detalle.getProducto().getId() === producto.getId();
    });
    if (detalleRepetido) {
      detalleRepetido.setCantidad(detalleRepetido.getCantidad() + 1);
    } else {
      this.detalles.push(new DetallePedido(producto, 1));
    }

  }

  limpiarCarrito() {
    this.detalles = [];
    return this.detalles;
  }
}
