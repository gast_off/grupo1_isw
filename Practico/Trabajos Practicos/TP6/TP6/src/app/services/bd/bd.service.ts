import { Injectable } from '@angular/core';
import { Producto } from '../../classes/producto';

@Injectable({
  providedIn: 'root'
})
export class BdService {
  productos: Producto[] = [];

  constructor() {
    this.productos.push(new Producto(1, "Hamburguesa", 500, "Hamburguesa de queso"));
    this.productos.push(new Producto(2, "Ensalada", 600, "Ensalada de lechuga y tomate"));
    this.productos.push(new Producto(3, "Empanada", 80, "Empanada de carne, papa y aceitunas"));
    this.productos.push(new Producto(4, "Torta", 1000, "Torta de chocolate"));
    this.productos.push(new Producto(5, "Lomito", 1200, "Carne, lechuga, tomate, huevo frito y mayonesa"));
    this.productos.push(new Producto(6, "Pizza", 1000, "Pizza Napolitana"));
    this.productos.push(new Producto(7, "Cono de papas", 200, "Papas McCain"));
    this.productos.push(new Producto(8, "Sushi", 2000, "Sushi completo"));
  }
}
