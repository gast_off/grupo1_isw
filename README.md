## Plan de Gestión de Configuración

### Estructura de carpetas propuesta

grupo1_isw
- Bibliografia
- Practico
    - Guias
    - Trabajos conceptuales
        - Pecha Kucha
        - Poster Cientifico
    - Trabajos Practicos
        - TP1
        - TP2
        - TP3
        - TP4
        - TP5
        - TP6
        - TP7
        - TP8
        - TP9
        - TP10
        - TP11
        - TP12
        - TP13
        - TP14
        - TP15
- Programa
- Teorico
    - Materiales Adicionales
    - Presentaciones

### URL de acceso al repositorio implementado

https://gitlab.com/gast_off/grupo1_isw.git

### Reglas de nombrado

| Nombre del Ítem de Configuración      | Regla de Nombrado                          | Ubicación Física |
|---------------------------------------|--------------------------------------------|------------------|
| Libro                                 | LB_\<Libro>.pdf                            | /Bibliografia    |
| Modalidad                             | MOD_ProgramaDeIngenieriaDeSoftware2022.pdf | /Programa        | 
| Grabación                             | ENL_Links_clases_2021.\<Extension>         | /Programa        | 
| Cronograma                            | CRON_Cronograma_propuesto2022.\<Extension> | /Programa        | 
| Guía                                  | G_\<Nombre_Guia>.pdf                       | /Practico/Guias  |  
| Template                              | TEMP_\<Nombre_Temp>.\<Extension>           | /Teorico/Materiales Adicionales |
| Presentación de clase                 | PRES_\<Nombre_Pres>.pdf                    | /Teorico/Presentaciones |
| Trabajo Práctico                      | TP\<NN>.\<Extension>                       | /Practico/Trabajos Practicos/TP\<NN> |
| Trabajo Práctico Conceptual           | TPC_\<Nombre_TPC>.\<Extension>             | /Practico/Trabajos conceptuales/\<Nombre_TPC> |
| Material Adicional Teórico            | MAT_\<Nombre_MAT>.\<Extension>             | /Teorico/Materiales Adicionales |
| Material Adicional Trabajos Prácticos | MAP_TP\<NN>_\<Nombre_MATP>.\<Extension>    | /Practico/Trabajos Practicos/TP\<NN> | 
| Material Pecha Kucha                  | PK_\<NombreArchivoPK>.\<Extension>         | /Practico/Trabajos Conceptuales/Pecha Kucha |
| Material Póster Científico            | PC_\<NombreArchivoPC>.\<Extension>         | /Practico/Trabajos Conceptuales/Poster Cientifico | 
| Bibliografía Recomendada              |  BR_BibliografiaRecomendada.pdf            | /Bibliografia    | 

| Sigla             | Significado                                                                   |
|-------------------|-------------------------------------------------------------------------------|
|\<Libro>           | Nombre del libro.                                                             |
|\<NN>              | N° cardinal comenzado en 1.                                                   |
|\<Nombre_TPC>      | Nombre de trabajo práctico conceptual.                                        |
|\<Extension>       | Nombre de la extensión de un archivo identificado como ítem de configuración. |
|\<Nombre_Temp>     | Nombre del template.                                                          |
|\<Nombre_Pres>     | Nombre que posee la presentación.                                             |
|\<Nombre_MAT>      | Nombre del material adicional teórico.                                        |
|\<Nombre_MATP>     | Nombre del material adicional práctico.                                       |
|\<NombreArchivoPK> | Nombre del archivo relacionado con el trabajo de Pecha Kucha.                 |
|\<NombreArchivoPC> | Nombre del archivo relacionado con el trabajo de Póster científico.           |
|\<Nombre_Guia>     | Nombre de la guía.

### Criterio para la creación de una línea base

En cuanto al momento que consideramos adecuado para marcar una línea base, tomamos la decisión de realizar una línea base luego de aplicar las correcciones ante la devolución de los profesores sobre las instancias evaluativas (Trabajos prácticos evaluables y parciales).


